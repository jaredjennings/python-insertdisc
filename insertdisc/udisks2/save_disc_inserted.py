import sys
import logging
import dbus
import pprint
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GObject
from insertdisc.udisks2.pickle_dbus import pickle_dbus_thing

OFUD2Path = '/org/freedesktop/UDisks2'

class OFUD2:
    TOP = 'org.freedesktop.UDisks2'
    Block = TOP + '.Block'
    Filesystem = TOP + '.Filesystem'
    Drive = TOP + '.Drive'

OFDOM = 'org.freedesktop.DBus.ObjectManager'
OFDP = 'org.freedesktop.DBus.Properties'
OFUDENACO = 'org.freedesktop.UDisks2.Error.NotAuthorizedCanObtain'

if __name__ == '__main__':
    FORMAT = '%(asctime)-15s %(levelname)s %(name)s %(message)s'
    logging.basicConfig(format=FORMAT, stream=sys.stderr, level=logging.DEBUG)

    DBusGMainLoop(set_as_default=True)

    # http://stackoverflow.com/questions/5067005/python-udisks-enumerating-device-information
    bus = dbus.SystemBus()
    ud_om_obj = bus.get_object(OFUD2.TOP, '/org/freedesktop/UDisks2')
    ud_om = dbus.Interface(ud_om_obj, OFDOM)
    ialog = logging.getLogger('interfaces')

    def added(object_path, interfaces_and_properties):
        ialog.info('InterfacesAdded to {}'.format(object_path))
        for interface in interfaces_and_properties.keys():
            ialog.info(' -- properties for interface %s', interface)
            getprops(object_path, interface)
            watchprops(object_path, interface)
    def removed(object_path, interfaces):
        ialog.info('InterfacesRemoved from {}'.format(object_path))
        for interface in interfaces:
            ialog.info(' -- removed: {}'.format(interface))
            unwatchprops(object_path, interface)

    def getprops(object_path, interface_name):
        p_obj = bus.get_object(OFUD2.TOP, object_path)
        p = dbus.Interface(p_obj, OFDP)
        props = p.GetAll(interface_name)
        pprint.pprint(pickle_dbus_thing(props))

    # to avoid watching an object twice if it has two interesting
    # interfaces
    properties_watched = set()
    
    def watchprops(object_path, interface_name):
        ialog.info('watching property changes on {}'.format(object_path))
        def propschanged(interface_name, changed, invalidated):
            ialog.info('Properties changed on object {}, interface {}'.format(
                object_path, interface_name))
            pprint.pprint(pickle_dbus_thing(changed))
            if invalidated:
                ialog.info('Properties invalidated on {}, interface {}'.format(
                    object_path, interface_name))
                pprint.pprint(pickle_dbus_thing(invalidated))
        if object_path not in properties_watched:
            bus.add_signal_receiver(propschanged, 'PropertiesChanged', OFDP,
                                    path=object_path)
            properties_watched.add(object_path)
    def unwatchprops(object_path, interface_name):
        if object_path in properties_watched:
            bus.remove_signal_receiver(propschanged, 'PropertiesChanged', OFDP,
                                       path=object_path)
            properties_watched.remove(object_path)

    bus.add_signal_receiver(added, 'InterfacesAdded', OFDOM,
                            path=OFUD2Path)
    bus.add_signal_receiver(removed, 'InterfacesRemoved', OFDOM,
                            path=OFUD2Path)

    logging.info('looping forever waiting for InterfacesAdded/InterfacesRemoved events')
    logging.info('to quit press Ctrl-C')
    loop = GObject.MainLoop()
    loop.run()
