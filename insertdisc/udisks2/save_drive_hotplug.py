import sys
import logging
import dbus
import pprint
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GObject
from insertdisc.udisks2.pickle_dbus import pickle_dbus_thing

OFUD2Path = '/org/freedesktop/UDisks2'

class OFUD2:
    TOP = 'org.freedesktop.UDisks2'
    Block = TOP + '.Block'
    Filesystem = TOP + '.Filesystem'
    Drive = TOP + '.Drive'

OFDOM = 'org.freedesktop.DBus.ObjectManager'
OFDP = 'org.freedesktop.DBus.Properties'
OFUDENACO = 'org.freedesktop.UDisks2.Error.NotAuthorizedCanObtain'

if __name__ == '__main__':
    FORMAT = '%(asctime)-15s %(levelname)s %(name)s %(message)s'
    logging.basicConfig(format=FORMAT, stream=sys.stderr, level=logging.DEBUG)

    DBusGMainLoop(set_as_default=True)

    # http://stackoverflow.com/questions/5067005/python-udisks-enumerating-device-information
    bus = dbus.SystemBus()
    ud_om_obj = bus.get_object(OFUD2.TOP, '/org/freedesktop/UDisks2')
    ud_om = dbus.Interface(ud_om_obj, OFDOM)

    def added(object_path, interfaces_and_properties):
        print('InterfacesAdded to {}'.format(object_path))
        print()
        pprint.pprint(pickle_dbus_thing(interfaces_and_properties))
        print()
        print()
    def removed(object_path, interfaces_and_properties):
        print('InterfacesRemoved from {}'.format(object_path))
        print()
        pprint.pprint(pickle_dbus_thing(interfaces_and_properties))
        print()
        print()

    bus.add_signal_receiver(added, 'InterfacesAdded', OFDOM,
                            path=OFUD2Path)
    bus.add_signal_receiver(removed, 'InterfacesRemoved', OFDOM,
                            path=OFUD2Path)

    logging.info('looping forever waiting for InterfacesAdded/InterfacesRemoved events')
    logging.info('to quit press Ctrl-C')
    loop = GObject.MainLoop()
    loop.run()
