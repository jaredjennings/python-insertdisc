from .pickle_dbus import pickle_dbus_thing, find_values
import dbus
import pprint

OFUD2Path = '/org/freedesktop/UDisks2'

class OFUD2:
    TOP = 'org.freedesktop.UDisks2'
    Block = TOP + '.Block'
    Filesystem = TOP + '.Filesystem'
    Drive = TOP + '.Drive'

OFDOM = 'org.freedesktop.DBus.ObjectManager'
OFDP = 'org.freedesktop.DBus.Properties'
OFUDENACO = 'org.freedesktop.UDisks2.Error.NotAuthorizedCanObtain'


if __name__ == '__main__':
    # http://stackoverflow.com/questions/5067005/python-udisks-enumerating-device-information
    bus = dbus.SystemBus()
    ud_om_obj = bus.get_object(OFUD2.TOP, '/org/freedesktop/UDisks2')
    ud_om = dbus.Interface(ud_om_obj, OFDOM)

    erthing = ud_om.GetManagedObjects()
    # WARNING: not sanitized
    pprint.pprint(pickle_dbus_thing(erthing))
