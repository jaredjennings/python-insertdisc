from insertdisc.udisks2.pickle_dbus import sanitize_udisks2
import sys
import pprint

with open(sys.argv[1]) as f:
    pprint.pprint(sanitize_udisks2(eval(f.read())))
