import dbus
import random
import re
from functools import reduce


class UnhandledPickle(Exception):
    """A value of an unexpected DBus type was encountered."""
    pass

def pickle_dbus_thing(x):
    """Turn dbus-typed objects into lists, dicts, numbers and strings."""
    if isinstance(x, dbus.Dictionary):
        return {pickle_dbus_thing(k): pickle_dbus_thing(v)
                for k, v in x.items()}
    elif isinstance(x, dbus.Array):
        if all(isinstance(y, dbus.Byte) for y in x):
            the_bytes = b''.join(bytes(b) for b in x).rstrip(b'\x00')
            # this is not certain: the reason it is a bytes is because
            # Unix doesn't nail down the encoding of its
            # filenames. but it will probably work.
            return the_bytes.decode('ascii')
        else:
            return [pickle_dbus_thing(y) for y in x]
    elif isinstance(x, dbus.ObjectPath):
        return str(x)
    elif isinstance(x, dbus.String):
        return str(x)
    elif isinstance(x, dbus.Boolean):
        return bool(x)
    elif isinstance(x, dbus.UInt32):
        return int(x)
    elif isinstance(x, dbus.UInt64):
        return int(x)
    elif isinstance(x, dbus.Int32):
        return int(x)
    elif isinstance(x, dbus.Int64):
        return int(x)
    elif isinstance(x, dbus.Double):
        return float(x)
    elif isinstance(x, dbus.Struct):
        return [pickle_dbus_thing(y) for y in x]
    else:
        raise UnhandledPickle(type(x), x)


# if i were really smart i'd have coded zippers here, or something
def find_values(needle, haystack, depth=0):
    """In a morass of lists and dicts, find values associated with a key.

    needle: the key, likely a string.
    haystack: the morass of lists and dicts.
    depth: used to limit recursion; do not supply.
    """
    if depth > 20:
        raise Exception('something has gone wrong with the recursion')
    if hasattr(haystack, 'items'):
        for k, v in haystack.items():
            if k == needle:
                if v != '':
                    yield v
            yield from find_values(needle, v, depth+1)
    else:
        if hasattr(haystack, 'strip'):
            # let's not iterate through this string-like thing
            pass
        else:
            try:
                for v in haystack:
                    yield from find_values(needle, v, depth+1)
            except (NameError, TypeError):
                # not iterable
                pass


def map_values(f, haystack, depth=0):
    """Modify each non-list, non-dict thing in a morass of lists and dicts.

    f: a function to apply to each non-list, non-dict thing.
    haystack: the morass of lists and dicts.
    depth: used to limit recursion; do not supply.
    """
    if depth > 20:
        raise Exception('something has gone wrong with the recursion')
    if hasattr(haystack, 'items'):
        return {k: map_values(f, v) for k,v in haystack.items()}
    else:
        if hasattr(haystack, 'strip'):
            # let's not iterate through this string-like thing
            return f(haystack)
        else:
            try:
                return [map_values(f, x, depth+1) for x in haystack]
            except (NameError, TypeError):
                # not iterable
                return f(haystack)

class DontKnowHowToRandomize(Exception):
    pass

def randomize(s):
    """Make a random identifier following the form of an existing one."""

    # these go in order from most restrictive to least restrictive.
    #
    # number
    if re.match('^[0-9]+$', s):
        return re.sub('[0-9]',
                      lambda c: random.choice('0123456789'),
                      s)
    elif re.match('^[0-9A-Z]+$', s):
        return re.sub('[0-9A-Z]',
                      lambda c: random.choice(
                          '0123456789'
                          'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
                      s)
    # uuid
    elif re.match('^([0-9a-f]+-*)+$', s):
        return re.sub('[0-9a-f]',
                      lambda c: random.choice('0123456789abcdef'),
                      s)
    # base64 uuid?
    elif re.match('^([0-9a-zA-Z]+-*)+$', s):
        return re.sub('[0-9a-zA-Z]',
                      lambda c: random.choice(
                          '0123456789'
                          'abcdefghijklmnopqrstuvwxyz'
                          'ABCDEFGHIJKLMNOPQRSTUVWXYZ'),
                      s)
    else:
        raise DontKnowHowToRandomize(s)


def sanitize_replacements(getall):
    salacious_attributes = ('IdUUID', 'Serial')
    replacements = {}
    for att in salacious_attributes:
        values = find_values(att, getall)
        for v in values:
            if v not in replacements:
                replacements[v] = randomize(v)
    return replacements

def multireplace_function(replacements):
    def replacer(s):
        if hasattr(s, 'replace'):
            for k, v in replacements.items():
                s = s.replace(k, v)
            return s
        else:
            return s
    return replacer


def sanitize_udisks2(getall):
    replacements = sanitize_replacements(getall)
    replacer = multireplace_function(replacements)
    return map_values(replacer, getall)
