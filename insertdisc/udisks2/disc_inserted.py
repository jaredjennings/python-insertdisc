import sys
import logging
import dbus
import pprint
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GObject
from insertdisc.udisks2.pickle_dbus import pickle_dbus_thing
from collections import defaultdict
from functools import partial
from threading import Lock

# distant credit: http://stackoverflow.com/questions/5067005/python-udisks-enumerating-device-information

class OFUD2:
    """Abbreviations for well-known strings."""
    TOP = 'org.freedesktop.UDisks2'
    TOP_PATH = '/org/freedesktop/UDisks2'
    Block = TOP + '.Block'
    Filesystem = TOP + '.Filesystem'
    Drive = TOP + '.Drive'
    @classmethod
    def shorten(c, s):
        if s.startswith(c.TOP):
            return 'OFUD2' + s[len(c.TOP):]
        return s
    @classmethod
    def shorten_path(c, s):
        if s.startswith(c.TOP_PATH):
            return '/OFUD2' + s[len(c.TOP_PATH):]
        return s

OFDOM = 'org.freedesktop.DBus.ObjectManager'
OFDP = 'org.freedesktop.DBus.Properties'
OFUDENACO = 'org.freedesktop.UDisks2.Error.NotAuthorizedCanObtain'

# these are so we can take properties that contain DBus object paths
# and point them at objects we've constructed corresponding to those
# DBus object paths - even when those objects don't exist yet. Sort of
# like a thunk, except that if it ever gets evaluated we throw an
# exception, instead of figuring out its value.
nothing_yet_sentinel = lambda: 42
class NotSetYet(Exception):
    pass

class DBusObject:
    """A DBus object we might be interested in.

    DBus objects for which instances of this class are created will
    always implement the properties interface. Some instances will
    pertain to fixed disks or flash media; these will contain the
    initial properties fetched, but we will not watch changes on them,
    and we won't end up using them.

    DBusObjects are constructed by the DBusObjectMaster.new method.

    Properties of the DBus object to which this Python object pertains
    are available as attributes of this object.

    """
    def __init__(self, **kwargs):
        self.object_path = kwargs['object_path']
        self.interface_name = kwargs['interface_name']
        self.master = kwargs['master']
        self.bus = kwargs['bus']
        self.log = logging.getLogger(repr(self))
        self.properties = {}
        self.watching_properties = False
        self.master.register(self)
        self.also_watch = []
    def __repr__(self):
        return '<{} {} at {}>'.format(
            self.__class__.__name__.split('.')[-1],
            OFUD2.shorten(self.interface_name),
            OFUD2.shorten_path(self.object_path))
    def properties_changed(self, interface_name, changed, invalidated):
        """Method called when DBus signals that properties have changed.

        This is the method passed to DBus' add_signal_receiver method.

        """
        self.update(changed)
        if invalidated:
            self.log.debug('Properties invalidated: %r', invalidated)
            for i in invalidated:
                # we shouldn't get a KeyError here, because all
                # properties set were directed by UDisks2
                del self.properties[i]
        self.master.a_change_happened(self)
    def update(self, properties):
        """A hook for subclasses to do special things on property updates."""
        self.properties.update(properties)
    def __getattr__(self, name):
        if name in self.properties:
            if self.properties[name] is nothing_yet_sentinel:
                raise NotSetYet(repr(self), name)
            else:
                return self.properties[name]
        else:
            # we only get called if the attribute wasn't already found
            # by the built-in means
            raise AttributeError(name)
    def get(self, *args):
        return self.properties.get(*args)
    def unregister(self):
        self.master.unregister(self)
    def please_also_watch(self, aw):
        self.also_watch.append(aw)
        if self.watching_properties:
            aw.watch_property_changes()
    def watch_property_changes(self):
        if not self.watching_properties:
            self.log.info('watching for property changes')
            self.bus.add_signal_receiver(self.properties_changed,
                                         'PropertiesChanged', OFDP,
                                         path=self.object_path)
            self.watching_properties = True
            for aw in self.also_watch:
                aw.watch_property_changes()
    def unwatch_property_changes(self):
        if self.watching_properties:
            self.log.debug('unwatching')
            self.bus.remove_signal_receiver(self.properties_changed,
                                            'PropertiesChanged', OFDP,
                                            path=self.object_path)
            self.watching_properties = False
            for aw in self.also_watch:
                aw.unwatch_property_changes()

class DBusHasBlock(DBusObject):
    """A DBusObject that has a Block associated with it.

    The attribute _Block is the associated DBusBlock object. It is
    filled in by the DBusBlock object, not this one.

    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.properties['_Block'] = nothing_yet_sentinel

class DBusDrive(DBusHasBlock):
    pass

class DBusBlock(DBusHasBlock):
    """An object to pertain to UDisks2 Block DBus objects.

    The OFUD2.Block DBus object has a Drive property, which contains
    the path of a OFUD2.Drive DBus object. This class makes the Drive
    attribute, which would normally contain the path, contain the
    associated DBusDrive object instead. It also makes the _Block
    attribute of the corresponding DBusDrive object point at this
    object.

    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.properties['_Filesystem'] = nothing_yet_sentinel
        self.properties['_Block'] = self
        self.master.request(self.object_path, OFUD2.Filesystem,
                            lambda ob: self.properties.update({
                                '_Filesystem': ob}))
    def update(self, properties):
        for k, v in properties.items():
            if k == 'Drive':
                if v == '/':
                    # this is the DBus path way of saying None.
                    # we will just not set the Drive attribute.
                    pass
                else:
                    self.properties[k] = nothing_yet_sentinel
                    def set_and_watch(ob):
                        self.properties.update({'Drive': ob})
                        ob.properties.update({'_Block': self})
                        ob.please_also_watch(self)
                    self.master.request(v, OFUD2.Drive, set_and_watch)
            else:
                self.properties[k] = v
    def unregister(self):
        if self.Drive not in [None, nothing_yet_sentinel]:
            self.Drive.unregister()
            self.Drive = None
        super().unregister()

class DBusObjectMaster:
    """Keep record of all DBusObjects, and hook them up to each other.

    It's this class that decides what kind of DBusObject to
    instantiate when it receives notification of a new possibly
    interesting DBUS object. And it fills requests made by the
    DBusBlock for references that don't exist till later.

    """
    def __init__(self, wi, bus):
        self.wi = wi
        self.bus = bus
        self.laters = defaultdict(lambda: []) # expect five at most

        self.add_if_added = {}
        self.log = logging.getLogger(self.__class__.__name__)
        self.objects = {} # expect dozens at most
    def register(self, ob):
        self.objects[ob.object_path] = ob
    def unregister(self, ob):
        if ob.object_path in self.objects:
            self.objects.pop(ob.object_path)
    def __contains__(self, path):
        return path in self.objects
    def new(self, object_path, interface, properties):
        interface_to_class = defaultdict(lambda: lambda: DBusObject, {
            OFUD2.Block: lambda: DBusBlock,
            OFUD2.Drive: lambda: DBusDrive,
        })
        klass = interface_to_class[interface]()
        ob = klass(object_path=object_path,
                   interface_name=interface,
                   properties=properties,
                   master=self, bus=self.bus)
        self.objects[object_path] = ob
        self.fill_laters()
        return ob
    def request(self, object_path, interface, supply):
        if object_path in self:
            ob = self.objects[object_path] 
            # should we check the interface?
            supply(ob)
        else:
            self.laters[(object_path, interface)].append(supply)
    def fill_laters(self):
        to_pop = []
        for (object_path, interface), supplies in self.laters.items():
            if object_path in self.objects:
                ob = self.objects[object_path]
                if ob.interface_name == interface:
                    for supply in supplies:
                        self.log.debug('supplying %r to %r', ob, supply)
                        supply(ob)
                    # we cannot modify the dict while iterating through it
                    to_pop.append((object_path, interface))
        for x in to_pop:
            self.laters.pop(x)
    def add(self, object_path, interface_name, properties):
        if object_path in self.objects:
            wo = self.objects[object_path]
        else:
            wo = self.new(object_path, interface_name, properties)
        # cold plug. if wo already existed, these are properties for a
        # new interface.
        wo.properties_changed(interface_name, properties, [])
    def watch(self, object_path):
        self.objects[object_path].watch_property_changes()
    def watch_later(self, object_path, interface, supply):
        self.laters.append((object_path, interface, supply))
    def is_slated(self, object_path, interface):
        # 
        return (object_path, interface) in self.laters
    def remove(self, object_path, interface):
        try:
            # this may raise the KeyError, preventing the
            # remove_signal_receiver
            wo = self.objects.pop(object_path)
            wo.unwatch_property_changes()
            # those waiting on this object should stop doing so
            try:
                del self.laters[(object_path, interface)]
            except KeyError:
                pass
        except KeyError:
            # we are already not watching this
            pass
    def a_change_happened(self, instigator):
        self.wi.a_change_happened(instigator)

class WatchedInterfaces:
    """Watches for interfaces to appear and disappear, and acts accordingly.

    DBus interfaces may appear when a device is connected and
    disappear when it is disconnected. But, for example, a Filesystem
    interface may also appear when a disc with a filesystem on it is
    inserted, and disappear when it is removed.

    Some set of interfaces will be around when we start, and
    WatchedInterfaces "cold-plugs" these, so the right objects and
    relationships will be constructed.

    """
    def __init__(self):
        self.getall = {}
        self.log = logging.getLogger('interfaces')
        self.bus = dbus.SystemBus()
        self.watchmaster = DBusObjectMaster(self, self.bus)
        self.listeners = []
        self.listeners_lock = Lock()

    def listen(self):
        ud_om_obj = self.bus.get_object(OFUD2.TOP, '/org/freedesktop/UDisks2')
        ud_om = dbus.Interface(ud_om_obj, OFDOM)
        erthing = ud_om.GetManagedObjects()
        # Listening before we deal with the initial events may result
        # in some duplicate processing. This should't be a problem
        self.bus.add_signal_receiver(
            self.interfaces_added,
            'InterfacesAdded', OFDOM, path=OFUD2.TOP_PATH)
        self.bus.add_signal_receiver(
            self.interfaces_removed,
            'InterfacesRemoved', OFDOM, path=OFUD2.TOP_PATH)
        self.log.debug('coldplugging %d objects', len(erthing.keys()))
        for object_path, interfaces_and_properties in erthing.items():
            self.log.info('coldplugging %s', object_path)
            self.interfaces_added(object_path, interfaces_and_properties)

    def interfaces_added(self, object_path, interfaces_and_properties):
        for interface, properties in interfaces_and_properties.items():
            self.watchmaster.add(object_path, interface, properties)
            if self.interface_interesting(object_path, interface, properties):
                self.log.info('Interesting: interface %s added to object %s',
                              interface, object_path)
                self.watchmaster.watch(object_path)

    def interfaces_removed(self, object_path, interfaces):
        self.log.info('InterfacesRemoved from {}'.format(object_path))
        for interface in interfaces:
            self.log.info(' -- removed: {}'.format(interface))
            self.watchmaster.remove(object_path, interface)

    def interface_interesting(self, object_path, interface, properties):
        if self.watchmaster.is_slated(object_path, interface):
            return True
        if interface == OFUD2.Drive:
            # find out if it is even an optical drive.
            is_optical = len([x for x in properties['MediaCompatibility']
                              if x.startswith('optical')]) > 0
            has_removable_media = bool(properties['MediaRemovable'])
            if is_optical and has_removable_media:
                return True
        return False
    def add_listener(self, listener):
        with self.listeners_lock:
            self.listeners.append(listener)
    def remove_listener(self, lid):
        with self.listeners_lock:
            self.listeners.remove(listener)
    def a_change_happened(self, instigator):
        with self.listeners_lock:
            for l in self.listeners:
                l(self.watchmaster, instigator)


class Condition:
    def __init__(self, **kwargs):
        self.log = kwargs['log']
    def __repr__(self):
        return '<{}>'.format(self.__class__.__name__)
    
def on_blank_media(mediatype, callbl, *args, **kwargs):
    """Call callbl when a blank medium of mediatype is available.

    This may be when the medium is inserted, or once at startup if
    there is already such a medium inserted.

    mediatype is defined by UDisks2, and is some string like
    'optical_cd_r'.

    The callable is called like callbl(x, *args, **kwargs) where x is
    a string representing the DBusObject whose change instigated this
    notification; and args and kwargs are the remaining arguments and
    the keyword arguments passed to this function.

    """
    log = logging.getLogger('on_blank_media')
    def propschanged(master, instigator):
        if isinstance(instigator, DBusDrive):
            if (instigator.get('MediaAvailable', False)
                and instigator.get('Optical', False)
                and instigator.get('OpticalBlank', False)
                and instigator.get('Media', '') == mediatype):
                log.info('calling callable, instigated by %r', instigator)
                callbl(repr(instigator), *args, **kwargs)
    return propschanged


def on_blank_media_get_dev(mediatype, callbl, *args, **kwargs):
    """Call callbl when a blank medium is in a device. Pass the device path.

    This may be when the medium is inserted, or once per device at
    startup if there is already such a medium inserted.

    mediatype is defined by UDisks2, and is some string like
    'optical_cd_r'. devicepath is something like '/dev/sr0'.

    The callable is called like callbl(devpath, x, *args, **kwargs)
    where devpath is a path to a device file (or maybe an empty
    string!) and x is a string representing the DBusObject whose
    change instigated this notification; and args and kwargs are the
    remaining arguments and the keyword arguments passed to this
    function.

    """
    log = logging.getLogger('on_blank_media_get_dev')
    def propschanged(master, instigator):
        try:
            if isinstance(instigator, DBusDrive):
                drive = instigator
                block = instigator._Block
# if we let the Block also be an instigator, the callable happens
# twice.
#
#            elif isinstance(instigator, DBusBlock):
#                drive = instigator.Drive
#                block = instigator
            else:
                return
            if (
                    drive.get('MediaAvailable', False) 
                    and drive.get('Optical', False)
                    and drive.get('OpticalBlank', False)
                    and drive.get('Media', '') == mediatype):
                log.info('calling callable, instigated by %r', instigator)
                callbl(block.Device, repr(instigator), *args, **kwargs)
        except AttributeError:
            pass
        except NotSetYet as e:
            # the conditions where this was a problem have been
            # mitigated. so we no longer need to warn anyone.
            pass
    return propschanged

    
def on_data_disc(callbl, *args, **kwargs):
    """Call callbl when any data disc is available.

    This may be when the disc is inserted, or once at startup if there
    is already such a disc inserted.

    The callable is called like callbl(x, *args, **kwargs) where x is
    a string representing the DBusObject whose change instigated this
    notification; and args and kwargs are the remaining arguments and
    the keyword arguments passed to this function.

    """
    log = logging.getLogger('on_data_disc')
    def propschanged(master, instigator):
        if isinstance(instigator, DBusDrive):
            if (instigator.get('Optical', False)
                and instigator.get('OpticalNumDataTracks', 0) > 0):
                log.info('calling callable, instigated by %r', instigator)
                callbl(repr(instigator), *args, **kwargs)
    return propschanged


def on_data_disc_get_label(callbl, *args, **kwargs):
    """Call callbl when a data disc is available, passing the volume label.

    This may be when the disc is inserted, or once at startup if there
    is already such a disc inserted.

    When a data disc is available, label_callbl is called with the
    label of the disc. If it returns True, callbl is called like
    callbl(x, *args, **kwargs), where x is a string representing the
    DBusObject whose change instigated this notification; and args and
    kwargs are the remaining arguments and the keyword arguments
    passed to this function.

    """
    log = logging.getLogger('on_data_disc_get_label')
    def propschanged(master, instigator):
        try:
# avoid calling callbl twice. it is the event that changes the Block
# that will set the IdLabel, so we ignore the Drive change.
#
#            if isinstance(instigator, DBusDrive):
#                drive = instigator
#                block = instigator._Block
            if isinstance(instigator, DBusBlock):
                drive = instigator.Drive
                block = instigator
            else:
                return
            if (
                    drive.get('Optical', False)
                    and drive.get('OpticalNumDataTracks', 0) > 0):
                log.info('calling callable, instigated by %r', instigator)
                callbl(block.IdLabel, repr(drive), *args, **kwargs)
        except AttributeError:
            pass
        except NotSetYet as e:
            # the conditions where this was a problem have been
            # mitigated. so we no longer need to warn anyone.
            pass
    return propschanged

def on_data_disc_compare_label(log, label_callbl, callbl, *args, **kwargs):
    """Call callbl when a certain data disc is available. See below.

    This may be when the disc is inserted, or once at startup if there
    is already such a disc inserted.

    When a data disc is available, label_callbl is called with the
    label of the disc. If it returns True, callbl is called like
    callbl(x, *args, **kwargs), where x is a string representing the
    DBusObject whose change instigated this notification; and args and
    kwargs are the remaining arguments and the keyword arguments
    passed to this function.

    """
    def propschanged(master, instigator):
        try:
            if isinstance(instigator, DBusDrive):
                drive = instigator
                block = instigator._Block
            elif isinstance(instigator, DBusBlock):
                drive = instigator.Drive
                block = instigator
            else:
                return
            if (
                    drive.get('Optical', False)
                    and drive.get('OpticalNumDataTracks', 0) > 0
                    and label_callbl(block.IdLabel)):
                log.info('calling callable, instigated by %r', instigator)
                callbl(repr(drive), *args, **kwargs)
        except AttributeError:
            pass
        except NotSetYet as e:
            # the conditions where this was a problem have been
            # mitigated. so we no longer need to warn anyone.
            pass
    return propschanged

def on_data_disc_labelled(label, callbl, *args, **kwargs):
    """Call callbl when a data disc having a given label is available.

    This may be when the disc is inserted, or once at startup if there
    is already such a disc inserted.

    callbl is called like callbl(x, *args, **kwargs), where x is a
    string representing the DBusObject whose change instigated this
    notification; and args and kwargs are the remaining arguments and
    the keyword arguments passed to this function.

    """
    log = logging.getLogger('on_data_disc_labelled(%r)' % label)
    def label_is(l):
        log.info('comparing label %r against %r', l, label)
        return l == label
    return on_data_disc_compare_label(log,
        label_is, callbl, *args, **kwargs)

def on_data_disc_not_labelled(label, callbl, *args, **kwargs):
    """Call callbl when a data disc not having a given label is available.

    This may be when the disc is inserted, or once at startup if there
    is already such a disc inserted.

    callbl is called like callbl(x, *args, **kwargs), where x is a
    string representing the DBusObject whose change instigated this
    notification; and args and kwargs are the remaining arguments and
    the keyword arguments passed to this function.

    """
    log = logging.getLogger('on_data_disc_not_labelled(%r)' % label)
    def label_isnt(l):
        log.info('comparing label %r against %r', l, label)
        if l == '':
            # no label, possibly meaning no filesystem on this disc,
            # should not trigger an event. if there is a filesystem,
            # there will shortly be an event that changes the label to
            # something, whereupon it can be sense-makingly checked.
            return False
        return l != label
    return on_data_disc_compare_label(log,
        label_isnt, callbl, *args, **kwargs)
