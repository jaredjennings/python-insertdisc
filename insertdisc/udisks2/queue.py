import sys
import logging
from dbus.mainloop.glib import DBusGMainLoop, threads_init
from gi.repository import GObject
from insertdisc.udisks2.disc_inserted import (
    WatchedInterfaces, on_blank_media_get_dev, on_data_disc_labelled)
from threading import Thread


def wait_for_disc_process(pipe):
    FORMAT = '%(asctime)-15s %(levelname)s %(name)s %(message)s'
    logging.basicConfig(format=FORMAT, stream=sys.stderr, level=logging.DEBUG)

    DBusGMainLoop(set_as_default=True)
    threads_init()
    
    wi = WatchedInterfaces()
    logging.info('gathering watching desires')
    while True:
        thing = pipe.recv()
        if thing[0] == 'go':
            break
        elif thing[0] == 'blank':
            mediatype = thing[1]
            wi.add_listener(on_blank_media_get_dev(
                mediatype, lambda devpath: pipe.send(('blank', devpath))))
        elif thing[0] == 'named_data':
            name = thing[1]
            wi.add_listener(on_data_disc_labelled(
                name, lambda devpath: pipe.send(('data', name, devpath))))
        else:
            # the on_data_disc* listeners do not yet permit to mount
            # the disc? i'm certain the groundwork is there
            raise ValueError('unrecognized desire', thing)
    wi.listen()

    logging.info('looping forever waiting for InterfacesAdded/InterfacesRemoved events')
    logging.info('to quit press Ctrl-C')
    loop = GObject.MainLoop()
    loop.run()
