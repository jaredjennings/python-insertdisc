import json
import dbus
import unittest
from insertdisc.udisks2.pickle_dbus import pickle_dbus_thing

class TestUdisks2GetAll(unittest.TestCase):
    def testDoesntCrash(self):
        bus = dbus.SystemBus()
        ud_om_obj = bus.get_object('org.freedesktop.UDisks2',
                                   '/org/freedesktop/UDisks2')
        ud_om = dbus.Interface(ud_om_obj,
                               'org.freedesktop.DBus.ObjectManager')
        erthing = ud_om.GetManagedObjects()
        # we don't do anything with this, we just try to make it and
        # hope it doesn't fail
        json.dumps(pickle_dbus_thing(erthing))

if __name__ == '__main__':
    unittest.main()
