import logging
import unittest
from unittest.mock import patch, MagicMock, sentinel

from insertdisc.udisks2.disc_inserted import (
    WatchedInterfaces, DBusObject, DBusBlock, DBusDrive,
    OFUD2, OFDOM, OFDP,
    on_blank_media, on_blank_media_get_dev,
    on_data_disc, on_data_disc_labelled,
    on_data_disc_not_labelled,
    on_data_disc_get_label)

from insertdisc.test.udisks2.getall import (
    no_discs, optical_drive_attached)

from insertdisc.test.udisks2.events import (
    optical_drive_hotplug, iso9660, blank)

class TestDiscInserted(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.next_inject_serial_number = 0
        
    def setUp(self):
        self.log = logging.getLogger('TestDiscInserted')
        self.log.info('--------- test begins: %s', self.id())
        self.systembus_patch = patch('insertdisc.udisks2.disc_inserted.dbus.SystemBus')
        self.interface_patch = patch('insertdisc.udisks2.disc_inserted.dbus.Interface')
        self.systembus = self.systembus_patch.start()
        self.interface = self.interface_patch.start()
        self.bus = self.systembus()
        self.wi = WatchedInterfaces()

    def tearDown(self):
        self.interface_patch.stop()
        self.systembus_patch.stop()
        self.log.info('---------- test ends: %s', self.id())

    def _injectPropertyChanges(self, theEvents):
        serno = self.next_inject_serial_number
        self.next_inject_serial_number += 1
        eventCount = len(theEvents)
        for i, (opath, intf, changed, invalidated) in enumerate(theEvents):
            self.log.debug('--- inject #%d: event %d of %d begins',
                           serno+1, i+1, eventCount)
            self.log.debug('    event: %s, a %s, props changed %r, '
                          'props invalidated %r',
                          opath, intf, changed, invalidated)
            wo = self.wi.watchmaster.objects[opath]
            wo.properties_changed(intf, changed, invalidated)
            self.log.debug('--- inject #%d: event %d of %d ends',
                           serno+1, i+1, eventCount)

    def testGetAll(self):
        """In beginning, all objects from UDisks2 are fetched:"""
        self.wi.listen()
        self.bus.get_object.assert_called_once_with(
            OFUD2.TOP, OFUD2.TOP_PATH)

    def testListenInterfaceChanges(self):
        """In beginning, interface changes are listened for:"""
        self.interface().GetManagedObjects.configure_mock(
            return_value=no_discs.getall)
        self.wi.listen()
        self.bus.add_signal_receiver.assert_any_call(
            self.wi.interfaces_added, 'InterfacesAdded',
            OFDOM, path=OFUD2.TOP_PATH)
        self.bus.add_signal_receiver.assert_any_call(
            self.wi.interfaces_removed, 'InterfacesRemoved',
            OFDOM, path=OFUD2.TOP_PATH)

    def testNoListenToNonOpticalDrives(self):
        self.interface().GetManagedObjects.configure_mock(
            return_value=no_discs.getall)
        self.wi.listen()
        self.assert_(not any(c for c
                              in self.bus.add_signal_receiver.call_args_list
                              if c[0][1] == 'PropertiesChanged'),
                     'signal receiver added for PropertiesChanged '
                     'on non-optical drives: interesting detector is faulty')
        
    def testColdPlugOpticalDrive(self):
        block_path = OFUD2.TOP_PATH+'/block_devices/sr0'
        drive_path = OFUD2.TOP_PATH+'/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA'
        self.interface().GetManagedObjects.configure_mock(
            return_value=optical_drive_attached.getall)
        self.wi.listen()
        block_object = self.wi.watchmaster.objects[block_path]
        drive_object = self.wi.watchmaster.objects[drive_path]
        self.bus.add_signal_receiver.assert_any_call(
            block_object.properties_changed, 'PropertiesChanged',
            OFDP, path=block_path)
        self.bus.add_signal_receiver.assert_any_call(
            drive_object.properties_changed, 'PropertiesChanged',
            OFDP, path=drive_path)

    def testHotPlugOpticalDrive(self):
        block_path = OFUD2.TOP_PATH+'/block_devices/sr0'
        drive_path = OFUD2.TOP_PATH+'/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA'
        self.interface().GetManagedObjects.configure_mock(
            return_value=no_discs.getall)
        self.wi.listen()
        # no optical disc drives initially, so we don't expect any
        # property changes listened for: just interfaces added and
        # removed.
        self.bus.add_signal_receiver.reset_mock()
        # now attach drive
        for object_path, interfaces_and_properties in (
                optical_drive_hotplug.added):
            self.wi.interfaces_added(object_path,
                                     interfaces_and_properties)
        block_object = self.wi.watchmaster.objects[block_path]
        drive_object = self.wi.watchmaster.objects[drive_path]
        self.bus.add_signal_receiver.assert_any_call(
            block_object.properties_changed, 'PropertiesChanged',
            OFDP, path=block_path)
        self.bus.add_signal_receiver.assert_any_call(
            drive_object.properties_changed, 'PropertiesChanged',
            OFDP, path=drive_path)

    def testUnplugOpticalDrive(self):
        block_path = OFUD2.TOP_PATH+'/block_devices/sr0'
        drive_path = OFUD2.TOP_PATH+'/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA'
        self.interface().GetManagedObjects.configure_mock(
            return_value=no_discs.getall)
        self.wi.listen()
        # now attach drive
        for object_path, interfaces_and_properties in (
                optical_drive_hotplug.added):
            self.wi.interfaces_added(object_path,
                                     interfaces_and_properties)
        block_object = self.wi.watchmaster.objects[block_path]
        drive_object = self.wi.watchmaster.objects[drive_path]
        self.bus.remove_signal_receiver.reset_mock()
        # now detach drive
        for object_path, interfaces in optical_drive_hotplug.removed:
            # testListenInterfaceChanges above tests that the
            # interfaces_removed method will be called by dbus at
            # the appropriate time
            self.wi.interfaces_removed(object_path, interfaces)
        self.bus.remove_signal_receiver.assert_any_call(
            block_object.properties_changed, 'PropertiesChanged',
            OFDP, path=block_path)
        self.bus.remove_signal_receiver.assert_any_call(
            drive_object.properties_changed, 'PropertiesChanged',
            OFDP, path=drive_path)
        self.assertEqual(len(self.bus.remove_signal_receiver.call_args_list), 2)

    def testInsertAnyDataDisc(self):
        ding = MagicMock()
        self.wi.add_listener(on_data_disc(ding))
        block_path = OFUD2.TOP_PATH+'/block_devices/sr0'
        drive_path = OFUD2.TOP_PATH+'/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA'
        self.interface().GetManagedObjects.configure_mock(
            return_value=optical_drive_attached.getall)
        self.wi.listen()
        ding.reset_mock()
        self._injectPropertyChanges(iso9660.disc_inserted)
        ding.assert_called_once_with(
            '<DBusDrive OFUD2.Drive at ' +
            OFUD2.shorten_path(drive_path) + '>')

    def testInsertAnyDataDiscGetLabel(self):
        ding = MagicMock()
        self.wi.add_listener(on_data_disc_get_label(ding))
        block_path = OFUD2.TOP_PATH+'/block_devices/sr0'
        drive_path = OFUD2.TOP_PATH+'/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA'
        self.interface().GetManagedObjects.configure_mock(
            return_value=optical_drive_attached.getall)
        self.wi.listen()
        ding.reset_mock()
        self._injectPropertyChanges(iso9660.disc_inserted)
        ding.assert_called_once_with(
            'Debian 8.3.0 amd64 1',
            '<DBusDrive OFUD2.Drive at ' +
            OFUD2.shorten_path(drive_path) + '>')

    def testInsertLabelledDataDisc(self):
        ding = MagicMock()
        dong = MagicMock()
        dang = MagicMock()
        dung = MagicMock()
        self.wi.add_listener(
            on_data_disc_labelled('Debian 8.3.0 amd64 1', ding))
        self.wi.add_listener(
            on_data_disc_not_labelled('Debian 8.3.0 amd64 1', dung))
        self.wi.add_listener(
            on_data_disc_labelled('Flarblezart', dong))
        self.wi.add_listener(
            on_data_disc_not_labelled('Flarblezart', dang))
        block_path = OFUD2.TOP_PATH+'/block_devices/sr0'
        drive_path = OFUD2.TOP_PATH+'/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA'
        self.interface().GetManagedObjects.configure_mock(
            return_value=optical_drive_attached.getall)
        self.wi.listen()
        ding.reset_mock()
        dong.reset_mock()
        dang.reset_mock()
        dung.reset_mock()
        self.log.info('%r properties: %r', self.wi.watchmaster.objects[block_path], self.wi.watchmaster.objects[block_path].properties)
        self.assertEqual(self.wi.watchmaster.objects[block_path].Drive.object_path,
                         self.wi.watchmaster.objects[drive_path].object_path)
        self._injectPropertyChanges(iso9660.disc_inserted)
        ding.assert_called_once_with(
            '<DBusDrive OFUD2.Drive at /OFUD2/drives/'
            'PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA>')
        dong.assert_not_called()
        dang.assert_called_once_with(
            '<DBusDrive OFUD2.Drive at /OFUD2/drives/'
            'PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA>')
        dung.assert_not_called()

    def testInsertBlankDisc(self):
        ding = MagicMock()
        self.wi.add_listener(on_blank_media('optical_cd_r', ding))
        block_path = OFUD2.TOP_PATH+'/block_devices/sr0'
        drive_path = OFUD2.TOP_PATH+'/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA'
        self.interface().GetManagedObjects.configure_mock(
            return_value=optical_drive_attached.getall)
        self.wi.listen()
        ding.reset_mock()
        self._injectPropertyChanges(blank.disc_inserted)
        ding.assert_called_once_with(
            '<DBusDrive OFUD2.Drive at /OFUD2/drives/'
            'PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA>')

    def testInsertBlankDiscGetDevice(self):
        ding = MagicMock()
        self.wi.add_listener(on_blank_media_get_dev('optical_cd_r', ding, 'a', b='c'))
        block_path = OFUD2.TOP_PATH+'/block_devices/sr0'
        drive_path = OFUD2.TOP_PATH+'/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA'
        self.interface().GetManagedObjects.configure_mock(
            return_value=optical_drive_attached.getall)
        self.wi.listen()
        ding.reset_mock()
        self._injectPropertyChanges(blank.disc_inserted)
        ding.assert_called_once_with(
            '/dev/sr0',
            '<DBusDrive OFUD2.Drive at /OFUD2/drives/'
            'PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA>',
            'a', b='c')

if __name__ == '__main__':
    import logging, sys
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    unittest.main()
