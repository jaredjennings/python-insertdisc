# no filesystem mounted.
#
# the properties invalidated were not saved, so i have filled them in
# with empty tuples
#
# form: (object path, interface, properties changed, properties
# invalidated)

disc_inserted = [
    ('/org/freedesktop/UDisks2/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA',
     'org.freedesktop.UDisks2.Drive',
     {'Media': 'optical_cd',
      'MediaAvailable': True,
      'Optical': True,
      'OpticalNumDataTracks': 1,
      'OpticalNumSessions': 1,
      'OpticalNumTracks': 1,
      'Size': 258998272,
      'TimeMediaDetected': 1467319461909660},
     ()),
    ('/org/freedesktop/UDisks2/block_devices/sr0',
     'org.freedesktop.UDisks2.Block',
     {'Id': 'by-uuid-2016-01-23-19-02-07-00',
      'IdLabel': 'Debian 8.3.0 amd64 1',
      'IdType': 'iso9660',
      'IdUUID': '2016-01-23-19-02-07-00',
      'IdUsage': 'filesystem',
      'IdVersion': 'Joliet Extension',
      'Size': 258998272,
      'Symlinks': ['', '', '', '', '']},
     ())]

disc_removed = [
    ('/org/freedesktop/UDisks2/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA',
     'org.freedesktop.UDisks2.Drive',
     {'Media': '',
      'MediaAvailable': False,
      'MediaCompatibility': ['optical_cd'],
      'Optical': False,
      'OpticalNumDataTracks': 0,
      'OpticalNumSessions': 0,
      'OpticalNumTracks': 0,
      'Size': 0,
      'TimeMediaDetected': 0},
     ()),
    ('/org/freedesktop/UDisks2/block_devices/sr0',
     'org.freedesktop.UDisks2.Block',
     {'Id': '',
      'IdLabel': '',
      'IdType': '',
      'IdUUID': '',
      'IdUsage': '',
      'IdVersion': '',
      'Size': 0,
      'Symlinks': ['', '']},
     ()),
    ('/org/freedesktop/UDisks2/drives/PIONEER_BD_RW___BDR_TD03_WZGA4IOMT5CA',
     'org.freedesktop.UDisks2.Drive',
     {'MediaCompatibility': ['optical_bd',
                             'optical_bd_r',
                             'optical_bd_re',
                             'optical_cd',
                             'optical_cd_r',
                             'optical_cd_rw',
                             'optical_dvd',
                             'optical_dvd_plus_r',
                             'optical_dvd_plus_r_dl',
                             'optical_dvd_plus_rw',
                             'optical_dvd_r',
                             'optical_dvd_ram',
                             'optical_dvd_rw',
                             'optical_mrw',
                             'optical_mrw_w']},
     ()),
    ('/org/freedesktop/UDisks2/block_devices/sr0',
     'org.freedesktop.UDisks2.Block',
     {'Symlinks': ['', '', '']},
     ())]
