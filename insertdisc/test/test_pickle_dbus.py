import unittest
import random
from insertdisc.udisks2.pickle_dbus import find_values, map_values, randomize
from insertdisc.test.udisks2.getall.no_discs import getall

class TestFindValues(unittest.TestCase):
    def testFindValues(self):
        self.assertEqual(set(find_values('IdUUID', getall)),
                         set([
                             'xh1PHI-oba5-y1WS-hzsK-5pJY-Ym4g-soHk0g',
                             '91596691-1f82-5d8d-78c6-efd2f94afd93',
                             'f30036f6-a17c-4f1c-ff1f-5a60118ac40b',
                             'd2eb8cff-d46a-c214-3e69-f46f51123095',
                             '25874476-83ed-af40-655e-af99db11d290',
                             'bfdd2916-af09-2448-8034-89a887ae2fde',
                         ]))

class TestRandomize(unittest.TestCase):
    def setUp(self):
        random.seed(42)
    def testRandomizeDecimalNumber(self):
        self.assertEqual(randomize('13571371'), '10433218')
    def testRandomizeUUID(self):
        self.assertEqual(randomize('07b033b8-d1d1-4af9-9a2a-7dccd79291f0'),
                         '30877432-d102-6706-d7e8-05da846a32c3')
    def testRandomizeLettersUUID(self):
        self.assertEqual(randomize('74qCK4-dLXS-JMNh-tpT6-JBL3-6B0P-xxW61P'),
                         'E71Lhf-e8L6-HLVy-5Br2-15de-wC1z-cJFIyq')


if __name__ == '__main__':
    unittest.main()
