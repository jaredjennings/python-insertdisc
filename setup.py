from setuptools import setup

setup(
        name='insertdisc',
        description='Provide "insert disc" functionality',
        long_description="""\
Bacon ipsum hodor synergize common ground claw owner
""",
        version='0.1',
        author='Jared Jennings',
        author_email='jjennings@fastmail.fm',
        license='BSD',
        platforms='Linux so far',
        packages=['insertdisc', 'insertdisc.udisks2'],
        )

